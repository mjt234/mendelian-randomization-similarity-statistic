# Mendelian Randomization Similarity Statistic

## Background
This repository will hold the R code used to produce the prospective manuscript _Do my sensitivity analyses agree? A statistical test to judge the similarity of several MR estimates_. This research was carried out by Matthew Tyler, Dr Ninon Mounier, and Professor Jack Bowden, building on the MSc dissertation of Matthew Tyler, undertaken at the University of Exeter.

## Status of project
The first submission of this manuscript is planned for late 2023 to early 2024, upon which an R script will be uploaded to this repository containing the code needed to replicate the results of the manuscript. At current, the associated code for this project is being cleaned, and the final updates to the manuscript draft are being made.

## Future developments
Pending acceptance of the prospective manuscript, an _R_ package will be constructed to apply the proposed similarity statistic to two-sample Mendelian Randomisation summary datasets.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
For any queries on the project please contact Matthew Tyler via either email (matthew.tyler@icr.ac.uk) or LinkedIn ([http://www.linkedin.com/in/matthew-tyler-273aa8166](url))

## Authors
Matthew Tyler (The Institute of Cancer Research, University of Exeter), Dr Ninon Mounier (University of Exeter), Professor Jack Bowden (University of Exeter).

## Acknowledgments
Professor Jack Bowden, Dr Ninon Mounier (projection supervisors)

Exeter Diabetes Group (ExCEED), College of Medicine and Health, University of Exeter, Exeter, U.K.

Mary Elliott-Davey, Amgen Inc. (funders of Matthew Tyler's MSc studies)

The Exeter Mendelian Randomization Group

Clinical Trials and Statistics Unit at The Institute of Cancer Research, London, U.K. (employer)

This work was supported by Cancer Research UK C1491
